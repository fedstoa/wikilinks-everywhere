const agora = "https://anagora.org"

browser.storage.local.set({ agora })
let selection
let lock = 0
const regexp = /\[\[(.*?)\]\]/g;

// this doesn't trigger here for some reason. permissions?
window.addEventListener("DOMContentLoaded", parsePage);

// these work
window.addEventListener("click", parsePage);
$("div,span,p").on("click", convertLinks);

function findElementByText(text) {
    var jSpot = $(":contains(" + text + ")")
        .filter(function () { return $(this).children().length === 0; })
        .filter(function () {
            const tag = $(this).prop("tagName")
            if (tag.match(/title/i)) {
                return 0
            }
            return 1
        })
    // .parent();  // because you asked the parent of that element
    return jSpot;
}

function convertLinks(e) {
    console.log("*** convertLinks")
    console.log(e.currentTarget)
    let bd = e.currentTarget.innerHTML
    const ar = [...bd.matchAll(regexp)];
    ar.forEach(a => {
        try {
            const slug = a[1].replace(/ /g, "-")
            const found = findElementByText(a[0])
            if (found.length == 0) return
            const html = found.html()
            const ret = html.replace(a[0], `<a href="${agora}/${slug}" target="_blank">[[${a[1]}]]</a>`)
            found.html(ret)
        } catch(e){
            console.error(e)
        }
 
    })
}

async function parsePage() {
    console.log("*** parsePage in app.js")
    if (lock) return
    lock = 1
    const txt = document.body.innerText
    const ar = [...txt.matchAll(regexp)];
    console.log("sending message", { ar })
    await browser.runtime.sendMessage(ar)
    lock = 0
}


function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}

// these doesn't trigger here for some reason. permissions?
window.addEventListener("DOMContentLoaded", parsePage);

document.addEventListener('selectionchange', () => {
    let select = document.getSelection().toString()
    if (select != "") {
        selection = select
    }
});

function makeList(term, target) {
    // const agora = "http://localhost:5000"
    const dropdown = document.getElementById("agora-dropdown")
    dropdown.innerHTML = ''



    fetch(`${agora}/similar/${term}.json`).then(res => res.json()).then(data => {
        for (const node of data) {
            const li = $(`<li ><div>${node}</div></li>`)
            // li.on("mouseover", e => {
            //     console.log("mouseover")
            //     console.log($(e.currentTarget).parent())
            //     $(e.currentTarget).parent().focus()
            // })
            li.on("click", e => {
                e.stopPropagation()
                e.preventDefault()

                console.log("click")
                const val = target.value || $(target).text()
                console.log(target.value, $(target).text())
                console.log({val})
                console.log({node})
                console.log({target})
                const newval = val.replace(`[[${term}`, `[[${node}]]`)
                console.log(newval)
                console.log(`jquery val ${$(target).val()} <-`)
                // $(target).focus()
                target.innerHTML = newval
                // $(target).putCursorAtEnd()
                dropdown.innerHTML = ''
            })
            dropdown.appendChild(li[0])
        }
    })

}

let timer

$(()=>{
    setTimeout(()=>{
        // let ce = $("div[contentEditable='true']")
        // ce.on("focus", ()=> console.log("wtf man"))
        // console.log(ce)
        // console.log("ready")

        $("textarea,div[contentEditable='true']").on("change keyup", e => {
            if(timer) clearTimeout(timer)
            console.log(e.currentTarget)
            const text = e.currentTarget.value || $(e.currentTarget).text()
            console.log(text)
            const matches = text.split(/\[\[/)
            console.log(matches)
            const last = matches[matches.length - 1]
            console.log(last)
            if (!last.match("]") && !last.match(/\[/) && last.length > 0) {
                console.log("last matches", last)
                timer = setTimeout(()=> makeList(last, e.currentTarget), 1000)
            }
            if (text.match(/\[\[v/)) {
            }
        
        })

        $("textarea,div[contentEditable='true']").on("focus", e => {
            console.log(`${e.currentTarget} has focus`)
            const pos = $(e.currentTarget).position()
            console.log(pos)
            $(e.currentTarget).after( $("#agora-dropdown"))
            // $("#agora-dropdown").offset({ top: pos.top, left: pos.left })
        })
    }, 1000)

})



const ul = $(`<ul id="agora-dropdown" style="z-index: 100;"></ul>`)
$("body").append(ul[0])


