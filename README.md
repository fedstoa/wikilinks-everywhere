# Wikilinks Everywhere

A [[web extension]] by [[vera]] and the [[flancia collective]]: <https://anagora.org/flancia-collective>.

## Install

### Firefox addon

Install at https://addons.mozilla.org/en-US/firefox/addon/agora/.

### Development
Development happens in Firefox, but it should work in Chromium based browsers as well (we'll try this later). In Firefox:

1. Clone this repository.
2. Go to about:debugging, click on 'This Firefox'.
3. 'Load Temporary Add-on...'
4. Select the directory where you cloned the repository.

## Use

- Click anywhere on a page to scan for wikilinks.
    - Then right click anywhere on a page to bring up a context menu that should include all [[wikilinks]] detected, resolved to a given context -- by default <https://anagora.org>.
- Click on a particular wikilink on the page to both re-scan and try to linkify it.
- Upcoming: press ctrl-shift-l to try to linkify all wikilinks.
